import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  Card,
  CardContent,
  CardHeader,
  Divider,
  makeStyles,
  Grid,
  Typography
  // useTheme
} from '@material-ui/core';

const useStyles = makeStyles(() => ({
  root: {
    height: '100%'
  }
}));

const IncomeStatement = ({ className, ...rest }) => {
  const classes = useStyles();
  // const theme = useTheme();

  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
    >
      <CardHeader title="Income Statement" />
      <Divider />
      <CardContent>
        <Grid
          container
          spacing={1}
          direction="column"
        >
          <Grid item>
            <Grid
              container
              justify="space-between"
            >
              <Typography
                variant="h6"
              >
                Revenue
              </Typography>
              <Typography
                variant="h6"
              >
                Rp. 1.306.507
              </Typography>
            </Grid>
          </Grid>
          <Grid item>
            <Grid
              container
              justify="space-between"
            >
              <Typography
                variant="h6"
              >
                COGS
              </Typography>
              <Typography
                variant="h6"
              >
                Rp. 208.453
              </Typography>
            </Grid>
          </Grid>
          <Divider />
          <Grid item>
            <Grid
              container
              justify="space-between"
            >
              <Typography
                variant="h6"
              >
                Gross Profit
              </Typography>
              <Typography
                variant="h6"
              >
                Rp. 1.097.064
              </Typography>
            </Grid>
          </Grid>
          <Grid item>
            <Grid
              container
              justify="space-between"
            >
              <Typography
                variant="h6"
              >
                Opex
              </Typography>
              <Typography
                variant="h6"
              >
                Rp. 815.306
              </Typography>
            </Grid>
          </Grid>
          <Grid item>
            <Grid
              container
              justify="space-between"
              style={{ paddingLeft: 16 }}
            >
              <Typography
                variant="body2"
              >
                Sales
              </Typography>
              <Typography
                variant="body2"
              >
                Rp. 279.886
              </Typography>
            </Grid>
          </Grid>
          <Grid item>
            <Grid
              container
              justify="space-between"
              style={{ paddingLeft: 16 }}
            >
              <Typography
                variant="body2"
              >
                Marketing
              </Typography>
              <Typography
                variant="body2"
              >
                Rp. 192.710
              </Typography>
            </Grid>
          </Grid>
          <Grid item>
            <Grid
              container
              justify="space-between"
              style={{ paddingLeft: 16 }}
            >
              <Typography
                variant="body2"
              >
                IT
              </Typography>
              <Typography
                variant="body2"
              >
                Rp. 192.656
              </Typography>
            </Grid>
          </Grid>
          <Grid item>
            <Grid
              container
              justify="space-between"
              style={{ paddingLeft: 16 }}
            >
              <Typography
                variant="body2"
              >
                General & Admin
              </Typography>
              <Typography
                variant="body2"
              >
                Rp. 150.054
              </Typography>
            </Grid>
          </Grid>
          <Grid item>
            <Grid
              container
              justify="space-between"
            >
              <Typography
                variant="h6"
              >
                Other Income
              </Typography>
              <Typography
                variant="h6"
              >
                Rp. 2.130
              </Typography>
            </Grid>
          </Grid>
          <Grid item>
            <Grid
              container
              justify="space-between"
            >
              <Typography
                variant="h6"
              >
                Other Expense
              </Typography>
              <Typography
                variant="h6"
              >
                Rp. 51.195
              </Typography>
            </Grid>
          </Grid>
          <Divider />
          <Grid item>
            <Grid
              container
              justify="space-between"
            >
              <Typography
                variant="h6"
              >
                Ebit
              </Typography>
              <Typography
                variant="h6"
              >
                Rp. 232.684
              </Typography>
            </Grid>
          </Grid>
          <Grid item>
            <Grid
              container
              justify="space-between"
              style={{ paddingLeft: 16 }}
            >
              <Typography
                variant="body2"
              >
                Interest and Tax
              </Typography>
              <Typography
                variant="body2"
              >
                Rp. 38.244
              </Typography>
            </Grid>
          </Grid>
          <Divider />
          <Grid item>
            <Grid
              container
              justify="space-between"
            >
              <Typography
                variant="h6"
              >
                Net Profit
              </Typography>
              <Typography
                variant="h6"
              >
                Rp. 194.440
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

IncomeStatement.propTypes = {
  className: PropTypes.string
};

export default IncomeStatement;
