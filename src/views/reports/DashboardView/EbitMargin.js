import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  Card,
  CardContent,
  Grid,
  Typography,
  colors,
  makeStyles
} from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100%',
    backgroundColor: colors.teal[400],
    color: colors.grey[50]
  }
}));

const EbitMargin = ({ className, ...rest }) => {
  const classes = useStyles();

  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
    >
      <CardContent>
        <Grid
          container
          justify="space-between"
          alignItems="center"
          spacing={1}
          direction="column"
        >
          <Grid item>
            <Typography
              variant="h5"
            >
              Ebit Margin
            </Typography>
          </Grid>
          <Grid item>
            <Typography
              variant="h1"
            >
              18%
            </Typography>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

EbitMargin.propTypes = {
  className: PropTypes.string
};

export default EbitMargin;
