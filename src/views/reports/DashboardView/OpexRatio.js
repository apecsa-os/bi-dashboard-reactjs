import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  Card,
  CardContent,
  Grid,
  Typography,
  colors,
  makeStyles
} from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100%',
    backgroundColor: colors.cyan[300],
    color: colors.grey[50]
  }
}));

const OpexRatio = ({ className, ...rest }) => {
  const classes = useStyles();

  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
    >
      <CardContent>
        <Grid
          container
          justify="space-between"
          alignItems="center"
          spacing={1}
          direction="column"
        >
          <Grid item>
            <Typography
              variant="h5"
            >
              Opex Ratio
            </Typography>
          </Grid>
          <Grid item>
            <Typography
              variant="h1"
            >
              74%
            </Typography>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

OpexRatio.propTypes = {
  className: PropTypes.string
};

export default OpexRatio;
