import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  Card,
  CardContent,
  Grid,
  Typography,
  colors,
  makeStyles
} from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100%',
    backgroundColor: colors.teal['A400'],
    color: colors.grey[50]
  }
}));

const NetProfitMargin = ({ className, ...rest }) => {
  const classes = useStyles();

  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
    >
      <CardContent>
        <Grid
          container
          justify="space-between"
          alignItems="center"
          spacing={1}
          direction="column"
        >
          <Grid item>
            <Typography
              variant="h5"
            >
              Net Profit Margin
            </Typography>
          </Grid>
          <Grid item>
            <Typography
              variant="h1"
            >
              14%
            </Typography>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

NetProfitMargin.propTypes = {
  className: PropTypes.string
};

export default NetProfitMargin;
